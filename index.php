<html>
  <head>
    <style>
       #map {
        width: 100%;
        height: 100%;
      }
      #floating-panel {
  position: absolute;
  left: 50%;
  z-index: 7;
  background-color: #00001a;
  opacity: 0.7;
  padding:5px;
  border: 1px solid #999;
  text-align: center;
  font-family: 'Roboto','sans-serif';

}
#leyenda{
  position: absolute;
  left:10px;
  top:45px;
  background-color: #00001a;
  opacity: 0.6;
    font-family: 'Roboto','sans-serif';
    z-index: 1;


}
#panelbuscando{
  position: absolute;
  left:10px;
  top:70%;

  opacity: 0.6;
    font-family: 'Roboto','sans-serif';
    z-index: 1;
}
#tabladirecciones{
    cursor:pointer;
}
#address{

 margin-right: 10px;
 width: 250px;
 height: 35px;
}
#labelciudad{
max-height: 45px;
max-width: 140px
}
#botonsubmit{
  max-height: 45px;
  max-width: 200px;
  background-color:#F7E600;
  color:#424242;
}
#rowsubmit{
  width: 200px;
}
#tablaleyenda{
    color:#ffffff;
}
#botonleyenda{
  position: absolute;
  left:10%;
  top:10px;
}
.popup{
  background-color: #BABAB4;
  opacity: 0.9;
  padding: 5px;
  border: 1px solid #999;
  text-align: center;
  font-family: 'Roboto','sans-serif';
  line-height: 30px;
  padding-left: 10px;
}
.cadarow{
  background-color:#F7E600;
  color: #424242;

}


    </style>
    <title>Despachador de Cabxi</title>
    <meta charset="UTF-8">
<meta name="description" content="Modulo de Dispatcher de Cabxi">
<meta name="keywords" content="Pi,Coders,Dispatcher,Despachadora,Operadora,Taxis,Transporte">
<meta name="author" content="Pi-Coders">
<meta name="idEmpresa" content="UIDEmpresa">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">
  </head>
  <body>

      <div class="alert fade in" id="notificacion-success" style="display:none;">
    <button type="button" class="close">×</button>
    Your error message goes here...
</div>

<div id="leyenda">
  <table class="table" id="tablaleyenda">
    <tr>
      <td>
        libre
      </td>
      <td>
<img src="otrotaxi.png" />
      </td>
    </tr>

    <tr>
      <td>
        Asignado
      </td>
      <td>
<img src="otrotaxiasignado.png" />
      </td>
    </tr>

    <tr>
      <td>
        Ocupado
      </td>
      <td>
<img src="otrotaxiocupado.png" />
      </td>
    </tr>

    <tr>
      <td>
        Deshabilitado
      </td>
      <td>
<img src="otrotaxideshabilitado.png" />
      </td>
    </tr>

    <tr>
      <td>
        Carrera Asignada
      </td>
      <td>
<img src="llamador.png" />
      </td>
    </tr>

    <tr>
      <td>
        Asignando Carrera
      </td>
      <td>
    <img src="llamador2.png" />
      </td>
    </tr>

  </table>
</div>

<div class="panel panel-default col-sm-2" id="panelbuscando">
  <div class="panel-heading">
    <h3 class="panel-title">Buscando...</h3>
  </div>
  <div class="panel-body" id="buscandobody">
    Panel content
  </div>
</div>


      <div id="floating-panel">
          <form id="formabuscar">
          <div class="input-group">
<table id="floating-table">
  <tr>
    <td>
  <input type="text" class="form-control input-sm" id="address" placeholder="Escriba Dirección" aria-describedby="basic-addon2">
</td>
<td>
  <span class="input-group-addon form-control" id="labelciudad">Popayán, Cauca</span>
</td>
<td id="rowsubmit">
   <input type="submit" class="btn form-control" id="botonsubmit" value="Buscar Dirección"></input>
</td>
 </tr>
 </table>
</div>

          <div><table class="table table-hover" id="tabladirecciones"></table></div>
</form>
    </div>

    </div>

    <div id="map"></div>
    <button type="button" class="btn btn-default" aria-label="Left Align" id="botonleyenda">
      <span class="glyphicon glyphicon-align-left" aria-hidden="true">Leyenda</span>
    </button>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script src="https://www.gstatic.com/firebasejs/3.1.0/firebase.js"></script>
        <script src="notify.js"></script>
        <script src='dispatcher.js' ></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBWpWAbUzaXEV9Iec-zbCHovC1fPvY7tc&signed_in=true&callback=initMap"
        async defer></script>


        <!-- include the style -->
        <!-- agregado por gabo!! :D -->
        <script src="https://www.gstatic.com/firebasejs/ui/live/0.4/firebase-ui-auth.js"></script>
<link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/live/0.4/firebase-ui-auth.css" />

  </body>
</html>
