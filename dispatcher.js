  var map,icon,iconasignado,iconocupado,icondesactivado,iconopos,iconbandera;
  var LISTA_TAXIS =[];
  var DIRECCION_ACTUAL=null;
  var LATLNG_ACTUAL=null;
  var CURRENT_DIRECCION_LATITUD=2.4365714;var CURRENT_DIRECCION_LONGITUD=-76.6172712;
  var POPAYAN_LATLNG;
  var directionsService ;
  var directionsDisplay;
  var CIRCULO1,CIRCULO2,CIRCULO3;
  var placesService;
  var LISTA_LATLNG_RESULTS;
  var MARKERPOSICION;
  var MARKERPOSICION_POPUP;
  var LISTA_POPUP=[];
  var TAXISESCOGIDOS=[];
  var SEGUNDOS_ESPERA=10;
  var RIPPLE_MARKER;
  var SWITCH_ASIGNAR=true;
  var SOLICITUD_ACTUAL;
  var CITYLAT,CITYLNG,CITYNOMBRE;
  var OPERADORA;
/*
Este es el codigo que debe ser modificado en el login
*/
var ID_EMPRESA=$('meta[name=idEmpresa]').attr('content');

console.log(ID_EMPRESA);
var styleArray = [
{
  featureType: "all",
  stylers: [
    { saturation: -80 }
  ]
},{
  featureType: "road.local",
  elementType: "geometry",
  stylers: [
    { hue: "#00ffee" },
    { saturation: 50 }
  ]
},{
  featureType: "road.arterial",
  elementType: "geometry",
  stylers: [
    { hue: "#ff9900" },
    { saturation: 50 }
  ]
},{
  featureType: "road.highway",
  elementType: "geometry",
  stylers: [
    { hue: "#cc0099" },
    { saturation: 50 }
  ]
},{
  featureType: "poi.business",
  elementType: "labels",
  stylers: [
    { visibility: "off" }
  ]
}
];

   var config = {
    apiKey: "AIzaSyCBWpWAbUzaXEV9Iec-zbCHovC1fPvY7tc",
    authDomain: "project-7807781103656514230.firebaseapp.com",
    databaseURL: "https://project-7807781103656514230.firebaseio.com",
    storageBucket: "project-7807781103656514230.appspot.com",
  };
  firebase.initializeApp(config);
//  service = new google.maps.places.PlacesService(map);

 var rootRef= firebase.database().ref();
 var taxisRef = firebase.database().ref("taxis");
 var empresaRef = firebase.database().ref("empresas/"+ID_EMPRESA);

 empresaRef.on("value",function(snapshot){
   var city=snapshot.val().ciudad;
   var image = snapshot.val().imagen;
   var name = snapshot.val().nombre;
//   OPERADORA = snapshot.val().usuarioOperadora;

//console.log(city,image,name,operadora);
   var ciudadref= firebase.database().ref("ciudades/"+city);
   ciudadref.on("value",function(snapshot2){
     var departamento=snapshot2.val().departamento;
     CITYLAT=snapshot2.val().latitud;
     CITYLNG=snapshot2.val().longitud;
     CITYNOMBRE=city+", "+departamento;
     console.log(CITYLAT,CITYLNG,CITYNOMBRE);
     map.panTo(new google.maps.LatLng(CITYLAT,CITYLNG));
     $('#labelciudad').text(CITYNOMBRE);
   });
 });



     window.initMap = function() {

  $('#panelbuscando').hide();
        var mapDiv = document.getElementById('map');
        map = new google.maps.Map(mapDiv, {
          //  center: {lat: CITYLAT, lng: CITYLNG},
            zoom: 14
        });
        map.setOptions({styles: styleArray});
        directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(map);
        directionsDisplay.setOptions( { suppressMarkers: true,preserveViewport: true } );

        iconbandera = {
        url: "llamador.png", // url
        scaledSize: new google.maps.Size(25, 25), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(12, 25) // anchor
        };

        var iconmarcador = {
          url: "llamador2.png", // url
          scaledSize: new google.maps.Size(25, 25), // scaled size
          origin: new google.maps.Point(0,0), // origin
          anchor: new google.maps.Point(12, 25) // anchor
        }
        var iconripple = {
          url: "ripple2.gif", // url
          scaledSize: new google.maps.Size(75, 75), // scaled size
          origin: new google.maps.Point(0,0), // origin
          anchor: new google.maps.Point(20, 20) // anchor

        }
        iconopos = {
              url: "callcabicon.png", // url
              scaledSize: new google.maps.Size(25, 25), // scaled size
              origin: new google.maps.Point(0, 0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
          };

    taxisRef.orderByChild("codigoEmpresa").equalTo(parseInt(ID_EMPRESA)).on("child_added", function (snapshot) {
        try{
            if(snapshot.val().idTaxista){
        var taxista_disponible = snapshot.val().disponible;
        var taxista_id = snapshot.val().idTaxista;
        var id_taxi = snapshot.key;
        var taxista_latitud = snapshot.val().latitud;
        var taxista_longitud = snapshot.val().longitud;
        var taxista_marca = snapshot.val().marca;
        var taxista_modelo = snapshot.val().modelo;
        var taxista_placa = snapshot.val().placa;
        var taxista_codigo_radio = snapshot.val().codigoRadio;
        var taxista_estado = snapshot.val().estado;
        var taxista_rumbo = snapshot.val().rumbo;




        var taxi1 = new Taxista(taxista_placa, taxista_latitud, taxista_longitud, map, id_taxi, taxista_id, taxista_codigo_radio, taxista_estado,taxista_rumbo);

        LISTA_TAXIS.push(taxi1);
        POPAYAN_LATLNG = new google.maps.LatLng(CURRENT_DIRECCION_LATITUD, CURRENT_DIRECCION_LONGITUD);
            }
        }catch(e){

        }
            //console.log(taxi1);  // Alerts "San Francisco"
    });
    MARKERPOSICION = new google.maps.Marker({
        icon: iconmarcador,
         draggable:true,
     });
     RIPPLE_MARKER =new google.maps.Marker({
         icon: iconripple,
         optimized: false

      });


     MARKERPOSICION.addListener('click', function () {

SWITCH_ASIGNAR=true;
for(var r=0;r<TAXISESCOGIDOS.length;r++){
  TAXISESCOGIDOS[r].tmp_ASKED=false;
}
     TAXISESCOGIDOS.length=0;
     escogerTaxiAutomaticamente();




    /*
       MARKERPOSICION_POPUP=  new google.maps.InfoWindow({
  //  content: generadorPopUp(pThis.IMAGEN,pThis.NOMBRE,pThis.CODIGO,pThis.PLACA,pThis.TELEFONO,pThis.ESTADO,null,null,pThis.ID_TAXISTA)
        content: CONTENIDO_POPUP
      });*/
     });

     $(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
      esconderRuta();
      esconderPopUps();
      e0sconderCirculos();
      DIRECCION_ACTUAL=null;
    }
});
    google.maps.event.addListener(map, "dblclick", function (e) {

        var direccion = obtenerDireccionFromLatLng(e.latLng);
        DIRECCION_ACTUAL = direccion;
        LATLNG_ACTUAL =e.latLng;
        CURRENT_DIRECCION_LONGITUD = e.latLng.lng();
        CURRENT_DIRECCION_LATITUD = e.latLng.lat();
      //  MARKERPOSICION.setPosition(e.latLng);
      //  MARKERPOSICION.setMap(map);
        try{
        LISTA_LATLNG_RESULTS.push(LATLNG_ACTUAL);
      }catch(e){

      }
        pintarCirculo(null,LATLNG_ACTUAL);




    });

    $('#botonleyenda').on('click',function(){
      $('#tablaleyenda').toggle()
    });

};
function escogerTaxiAutomaticamente(){

  organizarTaxisEncontrados();
  console.log(TAXISESCOGIDOS);
var tmpbool=false;
var taxi=null;
//if(TAXISESCOGIDOS.length>0)
taxi =TAXISESCOGIDOS[TAXISESCOGIDOS.length-1];
try{
  if(taxi.tmp_ASKED==true)
  tmpbool=true;

  taxi.tmp_ASKED=true;
}catch(e){

}

if(tmpbool || !SWITCH_ASIGNAR)
taxi=null;


//  TAXISESCOGIDOS.push(taxi);
console.log(tmpbool,SWITCH_ASIGNAR,TAXISESCOGIDOS.length);

try{
  $('#panelbuscando').show();
  RIPPLE_MARKER.setPosition(taxi.LATLNG);
  RIPPLE_MARKER.setMap(map);
  var solref = firebase.database().ref("solicitudes").child(taxi.SOLICITUD.ID).child("estado");
  solref.on("value",function(snapshot){
    if(snapshot.val()==="cancelado" && SWITCH_ASIGNAR){
  //    console.log("cancelando la solicidtud actual, proseguimos a preguntarle al proximo taxi");

      escogerTaxiAutomaticamente();

    }else if (snapshot.val()==="aceptado"){
      $.notify("Carrera asignada al taxista "+taxi.SOLICITUD.ID_TAXISTA+", a recoger pasagero en "+taxi.SOLICITUD.DIRECCION+" fué concluida con éxito","success");
        RIPPLE_MARKER.setMap(null);
        SWITCH_ASIGNAR=false;
        $('#panelbuscando').hide();

    }
  });

  //Esperamos algunos segundos a que el taxi responga
  var rootsol= firebase.database().ref("solicitudes").child(taxi.SOLICITUD.ID);
setTimeout(function(){
  console.log("cancelando espera despues de "+SEGUNDOS_ESPERA+" segundos");
  RIPPLE_MARKER.setMap(null);

  if(taxi.SOLICITUD.ESTADO=="pendiente"){
    rootsol.update({'estado':'cancelado'});

  }
},(SEGUNDOS_ESPERA*1000));

}catch(e){
  console.log(e);
  $('#buscandobody').html("<center><h4>No se hallaron Taxis dispuestos</h4></center>");
  RIPPLE_MARKER.setMap(null);
  MARKERPOSICION.setMap(null);
  setTimeout(function(){
    $('#panelbuscando').hide();
  },5000);


}

}
function organizarTaxisEncontrados(){
  var LISTA_CERCANOS= obtenerTaxisCercanos(MARKERPOSICION.position,1000);
  console.log("lista cercanos",LISTA_CERCANOS);
  for(var i=0;i<LISTA_CERCANOS.length;i++){
    console.log("preguntando por taxi",LISTA_CERCANOS[i]);
    if(TAXISESCOGIDOS.indexOf(LISTA_CERCANOS[i])==-1  ){
TAXISESCOGIDOS.push(LISTA_CERCANOS[i]);
      console.log("Asignando carrera a: ",LISTA_CERCANOS[i]);
      LISTA_CERCANOS[i].mostrarRuta(LATLNG_ACTUAL);
      $('#buscandobody').html("<center>Asignando Carrera a <h4>"+LISTA_CERCANOS[i].ID_TAXISTA+"</h4><br>Esperando Respuesta<br><button type='button' class='btn btn-danger' onClick='cancelarSolicitud()' id='cancelarSolicitud'>Cancelar</button></center>");
      $('#panelbuscando').show();

      asignarCarrera(LISTA_CERCANOS[i].ID_TAXISTA,'pendiente');
      console.log("asignada con exito");

      break;
    }else{
      console.log("Este Taxi ya ha sdo preguntado",LISTA_CERCANOS[i]);
    }
  }
//  return TAXISESCOGIDOS;
}
var Direccion = function (direccion, map) {
    this.DIRECCION = direccion;
    DIRECCION_ACTUAL = direccion;
    this.LATLNG = geocodeAddress(direccion, map);


};

var Solicitud = function (idsolicitud, otroThis) {
    this.ID = idsolicitud;
    this.HORA_ORIGEN = null;
    var parentThis = this;
    var solicitudRef = firebase.database().ref("solicitudes/" + idsolicitud);
    SOLICITUD_ACTUAL=idsolicitud;
    solicitudRef.once("value", function (snapshot) {
        if (snapshot.val()) {
            // console.log(snapshot.val());
            parentThis.CIUDAD = snapshot.val().ciudad;
            parentThis.DIRECCION = snapshot.val().direccion;
            var direccion = parentThis.DIRECCION;
            // parentThis.ID_USUARIO = snapshot.val().idUsuario;
            parentThis.ESTADO = snapshot.val().estado;
            parentThis.LATITUD = snapshot.val().latitud;
            parentThis.LONGITUD = snapshot.val().longitud;
            parentThis.LATLNG = new google.maps.LatLng(parentThis.LATITUD, parentThis.LONGITUD);

            var latlng = parentThis.LATLNG;
            parentThis.HORA_ORIGEN = snapshot.val().fecha_llegada_origen;
            var latitud = snapshot.val().latitud;
            var longitud = snapshot.val().longitud;
            var latlng = new google.maps.LatLng(latitud, longitud);
            var titulo = snapshot.val().direccion;
            parentThis.MARKER = new google.maps.Marker({
                position: latlng,
               map:map,
                title: titulo,
                icon:iconbandera
            });
            parentThis.MARKER.addListener('click', function () {
                otroThis.mostrarRuta(latlng);
            });

            var solicitudRef = firebase.database().ref("solicitudes/"+idsolicitud+"/estado");
             solicitudRef.on("value", function(snapshot) {
              //   console.log("Estado de la solicitud "+idsolicitud+" ha cambiado a "+snapshot.val());
                 parentThis.ESTADO=snapshot.val();
                 if(snapshot.val() === "cancelado" ){
                             parentThis.MARKER.setMap(null);
                             $.notify("Carrera del taxista "+parentThis.ID_TAXISTA+", a recoger pasagero en "+parentThis.DIRECCION+" fué cancelada","danger");
                             var newsolicitud=firebase.database().ref("solicitudes/"+idsolicitud);
                             newsolicitud.remove();
                             //$('#panelbuscando').hide();
                            // SWITCH_ASIGNAR=false;

                 }else if( snapshot.val()==="destino"){
                    parentThis.MARKER.setMap(null);
                              $.notify("Carrera asignada al taxista "+parentThis.ID_TAXISTA+", a recoger pasagero en "+parentThis.DIRECCION+" fué concluida con éxito","success");

                 }

             });

                     //



        }
    //    console.log(parentThis);
    });




};

      var PopUp = function(){

      };

      var Taxista = function ( placa, latitud, longitud,mapa,idtaxi,idtaxista,codigo,estado,rumbo) {
     //   this.NOMBRE = null;
        this.PLACA = placa;
        this.IMAGEN = null;
        this.TELEFONO = null;
        this.SOLICITUD=null;
        this.CODIGO = codigo;
        this.LATITUD = latitud;
        this.LONGITUD = longitud;
        this.ESTADO = estado;
        this.RUMBO = parseInt(rumbo);
        this.ESTADO=estado;
        var icono = icon;




        this.LATLNG = new google.maps.LatLng(latitud,longitud);
        this.OLD_LATLNG=this.LATLNG;
        this.MARKER = new google.maps.Marker({
                        position: this.LATLNG,
                        map: mapa,
                        icon: icono,

        });

        this.ID_TAXISTA = idtaxista;
        this.ID_TAXI =idtaxi;
        this.ID_SOLICITUD=null;
        var codigo=null;
        var telefono=null;
        var imagen;
        var nombre;
        var parentThis = this;
        var taxisRef = firebase.database().ref("taxis/"+this.ID_TAXI).orderByChild("idTaxista");
        var cuenta=0;
        taxisRef.on("value", function(snapshot) {
            parentThis.LATITUD = snapshot.val().latitud;
            parentThis.LONGITUD= snapshot.val().longitud;
            parentThis.RUMBO=snapshot.val().rumbo;


             var newlatlng = new google.maps.LatLng(parentThis.LATITUD,parentThis.LONGITUD);
          //   parentThis.LATLNG=newlatlng;
             if(snapshot.val().ciudad==="Popayan"){

             }
            if(cuenta>0){

                var index=LISTA_TAXIS.indexOf(parentThis);

               //  parentThis.MARKER.setMap(null);
               var tx = LISTA_TAXIS[index];

              console.log(tx);
          //     console.log(newlatlng.lat());
                // tx.MARKER.setPosition(newlatlng);
                  var xicono = tx.iconFromEstado(tx.ESTADO,tx.RUMBO,tx.ID_TAXI);
                tx.OLD_LATLNG = tx.LATLNG;
                 tx.LATLNG=newlatlng;
            //     if(!tx.ESTADO == (snapshot.val().estado)){
                //     tx.ESTADO = snapshot.val().estado;
                   tx.updateMarker(tx.LATLNG,xicono);
          //       }

      /*  if(index>-1)
        LISTA_TAXIS[index]=parentThis.MARKER;*/

         }
        cuenta++;

        });
        var marker = this.MARKER;


       var taxistaRef = firebase.database().ref("taxistas/"+idtaxista);
        taxistaRef.once("value").then(function(data){
         //    console.log(data.val());
         try{
           parentThis.NOMBRE = data.val().nombre;
           nombre=data.val().nombre;
           parentThis.IMAGEN = data.val().imagen;
           imagen = data.val().imagen;
           parentThis.TELEFONO=data.val().telefono;
           telefono=data.val().telefono;
           parentThis.ESTADO =data.val().estado;
           icono = parentThis.iconFromEstado(parentThis.ESTADO,parentThis.RUMBO,parentThis.ID_TAXI);
           parentThis.updateMarker(parentThis.LATLNG,icono);
           parentThis.ID_SOLICITUD = data.val().solicitudActual;
           if(parentThis.ID_SOLICITUD!=null)
           parentThis.SOLICITUD = new Solicitud(parentThis.ID_SOLICITUD,parentThis);

         var CONTENIDO_POPUP=parentThis.generadorPopUp();
           //     console.log(CONTENIDO_POPUP);
                  parentThis.POPUP =  new google.maps.InfoWindow({
          //  content: generadorPopUp(pThis.IMAGEN,pThis.NOMBRE,pThis.CODIGO,pThis.PLACA,pThis.TELEFONO,pThis.ESTADO,null,null,pThis.ID_TAXISTA)
           content: CONTENIDO_POPUP
        });
             LISTA_POPUP.push(parentThis.POPUP);
       }catch(e){

       }

      //     console.log("informacion de taxista cambio: "+data.val());

        });
        taxistaRef.child("estado").on("value", function (snapshot) {

            parentThis.updateMarker(parentThis.LATLNG,parentThis.iconFromEstado(snapshot.val(),parentThis.RUMBO),parentThis.ID_TAXI);
            parentThis.ESTADO=snapshot.val();
            console.log("cambiando estado de ",parentThis.ID_TAXISTA);
        });/*
        taxistaRef.child("latitud").on("value", function (snapshot) {

parentThis.LATLNG=new google.maps.LATLNG(snapshot.val(),parentThis.LONGITUD);
            parentThis.updateMarker(parentThis.LATLNG,parentThis.iconFromEstado(parentThis.ESTADO,parentThis.RUMBO),parentThis.ID_TAXI);
            parentThis.LATITUD=snapshot.val();
            console.console.log("cambiando posicion de ",parentThis.ID_TAXISTA);

        });
        taxistaRef.child("longitud").on("value", function (snapshot) {

          parentThis.LATLNG=new google.maps.LATLNG(parentThis.LATITUD,snapshot.val());
                      parentThis.updateMarker(parentThis.LATLNG,parentThis.iconFromEstado(parentThis.ESTADO,parentThis.RUMBO),parentThis.ID_TAXI);
                      parentThis.LONGITUD=snapshot.val();
        });
        taxistaRef.child("rumbo").on("value", function (snapshot) {
            parentThis.RUMBO=snapshot.val();
            parentThis.updateMarker(parentThis.LATLNG,parentThis.iconFromEstado(snapshot.val(),parentThis.RUMBO),parentThis.ID_TAXI);
            console.log("cambiando rumbo de ",parentThis.ID_TAXISTA);

        });*/


        var RotateIcon = function(options){
    this.options = options || {};
    this.rImg = options.img || new Image();
    this.rImg.src = this.rImg.src || this.options.url || '';
    this.options.width = this.options.width || this.rImg.width || 52;
    this.options.height = this.options.height || this.rImg.height || 60;
    canvas = document.createElement("canvas");
    canvas.width = this.options.width;
    canvas.height = this.options.height;
    this.context = canvas.getContext("2d");
    this.canvas = canvas;
};
RotateIcon.makeIcon = function(url) {
    return new RotateIcon({url: url});
};
RotateIcon.prototype.setRotation = function(options){
    var canvas = this.context,
        angle = options.deg ? options.deg * Math.PI / 180:
            options.rad,
        centerX = this.options.width/2,
        centerY = this.options.height/2;

    canvas.clearRect(0, 0, this.options.width, this.options.height);
    canvas.save();
    canvas.translate(centerX, centerY);
    canvas.rotate(angle);
    canvas.translate(-centerX, -centerY);
    canvas.drawImage(this.rImg, 0, 0);
    canvas.restore();
    return this;
};
RotateIcon.prototype.getUrl = function(){
    return this.canvas.toDataURL('image/png');
};






};
Taxista.prototype.moveMarker=function(deltalat,deltalng){
  var positionlat=0;
  var positionlng=0;
  var pThis=this;
var count=0;

    setTimeout(function(){
      if(count<10){
      try{

      positionlat = pThis.OLD_LATLNG.lat()+deltalat;
      positionlng =pThis.OLD_LATLNG.lng()+deltalng;

      var latlng = new google.maps.LatLng(positionlat, positionlng);
    //  console.log(latlng);
      pThis.MARKER.setPosition(latlng);
    count++;
    }catch(e){

    }
  }
 }, 100);




}
Taxista.prototype.updateMarker= function(latlng,icono){

  if(icono)
    this.MARKER.setIcon(icono);
  /*  var deltas=10;
    var deltalat =(latlng.lat()-this.OLD_LATLNG.lat())/deltas;
      var deltalng =(latlng.lng()-this.OLD_LATLNG.lng())/deltas;*/
    //  console.log(deltalat,deltalng);
    //  this.moveMarker(deltalat,deltalng)
    this.MARKER.setPosition(latlng);
  //  console.log(latlng.lat());
    //map.panTo(latlng);
    var pThis = this;

         this.MARKER.addListener('click', function() {


             if(pThis.SOLICITUD!=null){
               try{
                 pThis.mostrarRuta(pThis.SOLICITUD.LATLNG);
               }catch(e){

               }
              //   console.log(pThis.SOLICITUD);
             }
             if(LATLNG_ACTUAL != null){
               try{
                 pThis.mostrarRuta(LATLNG_ACTUAL);
               }catch (e){

               }
              //   console.log(LATLNG_ACTUAL);
             }

        for(var p=0;p<LISTA_POPUP.length;p++){
            LISTA_POPUP[p].close();
        }

//pThis.MARKER.setMap(null);
//pThis.MARKER.setMap(map);
      pThis.POPUP.setContent(pThis.generadorPopUp());
      pThis.POPUP.open(map, pThis.MARKER);

        });
};
 Taxista.prototype.generadorPopUp= function (){
     var TAXI = this;
    this.POPUP_HTML;
   // console.log(TAXI);
    if(TAXI.ESTADO=='libre' && DIRECCION_ACTUAL !=null){
    this.POPUP_HTML = '<div id="content" class="popup">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<div><table class="table table-hover"><tr><td><img src="'+TAXI.IMAGEN+'" width="200" height="200" /></td>\n\
<td><table class="table table-hover"><tr class="cadarow"><td>Nombre</td><td>'+TAXI.NOMBRE+'</td></tr><tr class="cadarow"><td>Código:</td><td>'+TAXI.CODIGO+'</td></tr><tr class="cadarow"><td>Placa:</td><td>'+TAXI.PLACA+'</td></tr><tr class="cadarow"><td>Teléfono:</td><td>'+TAXI.TELEFONO+'</td></tr><tr class="cadarow"><td><button type="button" class="btn btn-danger">Desactivar</button></td><td><button type="button" class="btn btn-success asignar" onClick="asignarCarrera(\''+TAXI.ID_TAXISTA+'\',\'aceptado\')">Asignar Carrera</button></td></tr></table> </td>\n\
</tr></table><div>\n\
</div></div>';
    }else if(TAXI.ESTADO==='asignado' && TAXI.SOLICITUD!=null){
         this.POPUP_HTML = '<div id="content"  class="popup">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<div><table class="table table-hover"><tr><td><img src="'+TAXI.IMAGEN+'" width="200" height="200" /></td>\n\
<td><table class="table table-hover"><tr class="cadarow"><td>Nombre</td><td>'+TAXI.NOMBRE+'</td></tr><tr class="cadarow"><td>Código:</td><td>'+TAXI.CODIGO+'</td></tr><tr class="cadarow"><td>Placa:</td><td>'+TAXI.PLACA+'</td></tr><tr class="cadarow"><td>Teléfono:</td><td>'+TAXI.TELEFONO+'</td></tr><tr class="cadarow"><td>Recogiendo cliente en:</td><td>'+TAXI.SOLICITUD.DIRECCION+' </td></tr></table> </td>\n\
</tr></table><div>\n\
</div></div>';

    }else if(TAXI.ESTADO==='fuera de servicio'){
          this.POPUP_HTML = '<div id="content"  class="popup">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<div><table class="table table-hover"><tr><td><img src="'+TAXI.IMAGEN+'" width="200" height="200" /></td>\n\
<td><table class="table table-hover"><tr class="cadarow"><td>Nombre</td><td>'+TAXI.NOMBRE+'</td></tr><tr class="cadarow"><td>Código:</td><td>'+TAXI.CODIGO+'</td></tr><tr class="cadarow"><td>Placa:</td><td>'+TAXI.PLACA+'</td></tr><tr class="cadarow"><td>Teléfono:</td><td>'+TAXI.TELEFONO+'</td></tr><tr class="cadarow"><td>Taxi esta fuera de Servicio</td><td><button type="button" class="btn btn-success">Reestablecer</button></td></tr></table> </td>\n\
</tr></table><div>\n\
</div></div>';
    }else if(TAXI.ESTADO ==='ocupado' && TAXI.SOLICITUD!=null){
               this.POPUP_HTML = '<div id="content"  class="popup">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<div><table class="table table-hover"><tr><td><img src="'+TAXI.IMAGEN+'" width="200" height="200" /></td>\n\
<td><table class="table table-hover"><tr class="cadarow"><td>Nombre</td><td>'+TAXI.NOMBRE+'</td></tr><tr class="cadarow"><td>Código:</td><td>'+TAXI.CODIGO+'</td></tr><tr class="cadarow"><td>Placa:</td><td>'+TAXI.PLACA+'</td></tr><tr class="cadarow"><td>Teléfono:</td><td>'+TAXI.TELEFONO+'</td></tr><tr class="cadarow"><td>Recogiendo cliente en:</td><td>'+TAXI.SOLICITUD.DIRECCION+' </td></tr><tr class="cadarow"><td>Hora de Recogida</td><td>'+TAXI.SOLICITUD.HORA_ORIGEN+'</td></tr></table> </td>\n\
</tr></table><div>\n\
</div></div>';
    }else if (TAXI.ESTADO === 'libre' && DIRECCION_ACTUAL==null){
                 this.POPUP_HTML = '<div id="content"  class="popup">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<div><table class="table table-hover"><tr><td><img src="'+TAXI.IMAGEN+'" width="200" height="200" /></td>\n\
<td><table class="table table-hover"><tr class="cadarow"><td>Nombre</td><td>'+TAXI.NOMBRE+'</td></tr><tr class="cadarow"><td>Código:</td><td>'+TAXI.CODIGO+'</td></tr><tr class="cadarow"><td>Placa:</td><td>'+TAXI.PLACA+'</td></tr><tr class="cadarow"><td>Teléfono:</td><td>'+TAXI.TELEFONO+'</td></tr></table> </td>\n\
</tr></table><div>\n\
</div></div>';
    }
    return(this.POPUP_HTML);

      //return contentString;

};
Taxista.prototype.mostrarRuta=function (latlng){
//console.log(latlng);
    var request = {
           origin: this.LATLNG,
           destination: latlng,
           travelMode: google.maps.DirectionsTravelMode.DRIVING
         };
         var tmpthis=this;
   //      clearMarkers();
   //      showMarkers();
         directionsService.route(request, function(response, status) {
           if (status == google.maps.DirectionsStatus.OK) {
            //   console.log(response);
             directionsDisplay.setDirections(response);
             tmpthis.tmp_DISTANCIA_NAVEGACION=directionsDisplay.directions.routes[0].legs[0].distance.value;
          //   console.log("distancia navegacion",tmpthis.tmp_DISTANCIA_NAVEGACION);
           }
         });


};
Taxista.prototype.iconFromEstado= function(estado,rotation,idtaxi){

  var otrotaxi="libre/otrotaxi";
  var taxiasignado="asignado/otrotaxiasignado";
  var taxiocupado="ocupado/otrotaxiocupado";
  var taxideshabilitado="deshabilitado/otrotaxiodeshabilitado";
  var medidor=".png";

  var ancho=15;
  var alto=35;
  if(rotation>=23 && rotation <68){
  medidor="45.png";
  ancho=35;
  alto=35;
}else if(rotation>=68 && rotation <112){
  medidor="90.png";
  ancho=35;
  alto=15;
}  else if(rotation>=112 && rotation<157){
  medidor="135.png";
  ancho=35;
  alto=35;
}else if(rotation>=157 && rotation<202){
  medidor="180.png";
  ancho=15;
  alto=35;
}else if(rotation>=202 && rotation<247){
  medidor="225.png";
  ancho=35;
  alto=35;
}  else if(rotation>=247 && rotation<292){
  medidor="270.png";
  alto=15;
  ancho=35;
}else if(rotation>=292 && rotation<337){
  medidor="315.png";
  alto=35;
  ancho=35;
}
  //console.log("rotation",rotation,"medidor",medidor);
  icon = {
url: otrotaxi+medidor, // url
scaledSize: new google.maps.Size(ancho, alto), // scaled size
origin: new google.maps.Point(0,0), // origin
anchor: new google.maps.Point(0, 0), // anchor

};

iconasignado = {
url: taxiasignado+medidor, // url
scaledSize: new google.maps.Size(ancho, alto), // scaled size
origin: new google.maps.Point(0,0), // origin
anchor: new google.maps.Point(0, 0), // anchor
};
iconocupado = {
url: taxiocupado+medidor, // url
scaledSize: new google.maps.Size(ancho, alto), // scaled size
origin: new google.maps.Point(0,0), // origin
anchor: new google.maps.Point(0, 0), // anchor
};
icondesactivado = {
url: taxideshabilitado+medidor, // url
scaledSize: new google.maps.Size(ancho, alto), // scaled size
origin: new google.maps.Point(0,0), // origin
anchor: new google.maps.Point(0, 0), // anchor
};

var tmpicon ="otrotaxi.png";
var tmpasignado="otrotaxiasignado.png";
var tmpocupado ="otrotaxiocupado.png";
var tmpdeshabilitado ="otrotaxideshabilitado.png";


     if(estado === 'libre')
            return icon;
        else if(estado==='asignado')
            return iconasignado;
        else if(estado==='ocupado')
            return iconocupado;
        else if (estado==='fuera de servicio')
            return icondesactivado;
};


Taxista.prototype.asignarSolicitud= function(idsolicitud,idtaxista){
    if(idsolicitud.length>0)
    this.SOLICITUD = new Solicitud(idsolicitud,this);
    var updatetaxista = firebase.database().ref("taxistas/"+idtaxista);
    updatetaxista.update({'estado':'asignado','solicitudActual':idsolicitud});
    for(var p=0;p<LISTA_POPUP.length;p++){
            LISTA_POPUP[p].close();
        }
};



  $( "#formabuscar" ).submit(function( event ) {
  event.preventDefault();
      var direccion = document.getElementById('address').value;
      DIRECCION_ACTUAL=direccion;
      var circulo = new Direccion(direccion+" ,"+CITYNOMBRE+", Colombia",map);
      for(var p=0;p<LISTA_POPUP.length;p++){
            LISTA_POPUP[p].close();
        }
     // getPlaceLatLng(direccion);
  });
  function esconderCirculos(){
    if(CIRCULO1 && CIRCULO2 && CIRCULO3){
        CIRCULO1.setMap(null);
        CIRCULO2.setMap(null);
        CIRCULO3.setMap(null);
    }
    MARKERPOSICION.setMap(null);

  }
  function esconderRuta(){
    directionsDisplay.setMap(null);
  }
  function esconderPopUps(){
    for(var p=0;p<LISTA_POPUP.length;p++){
        LISTA_POPUP[p].close();
    }

  }
function pintarCirculo(pos,latlng){

         if(latlng==null)
         var location = LISTA_LATLNG_RESULTS[pos];
         else
          var location=latlng
         LATLNG_ACTUAL=location;
         console.log("Pintando circulo en: "+location);
           CURRENT_DIRECCION_LATITUD=parseFloat(location.lat());
           CURRENT_DIRECION_LONGITUD=parseFloat(location.lng());
           MARKERPOSICION.setPosition(location);

  esconderCirculos();
  MARKERPOSICION.setMap(map);
         CIRCULO1 = new google.maps.Circle({
            strokeColor: '#BABAB4',
            strokeOpacity: 0.7,
            strokeWeight: 0.5,
            fillColor: '#F7E600',
            fillOpacity: 0.2,
            map: map,
            center:location,
            radius: 1000
        });
                CIRCULO2 = new google.maps.Circle({
                  strokeColor: '#BABAB4',
                  strokeOpacity: 0.7,
                  strokeWeight: 0.5,
                  fillColor: '#F7E600',
                  fillOpacity: 0.2,
            map: map,
            center: location,
            radius: 500
        });

                CIRCULO3 = new google.maps.Circle({
                  strokeColor: '#BABAB4',
                  strokeOpacity: 0.7,
                  strokeWeight: 0.5,
                  fillColor: '#F7E600',
                  fillOpacity: 0.2,
            map: map,
            center: location,
            radius: 150
        });
        map.panTo(location);
        document.getElementById('tabladirecciones').innerHTML="";

        esconderPopUps();
                google.maps.event.addListener(
                        MARKERPOSICION,
                        'dragend',
                        function () {
                            direccion = obtenerDireccionFromLatLng(MARKERPOSICION.position);
                            DIRECCION_ACTUAL = direccion;
                            LATLNG_ACTUAL = MARKERPOSICION.position;
                            CURRENT_DIRECCION_LONGITUD = MARKERPOSICION.position.lng();
                            CURRENT_DIRECCION_LATITUD = MARKERPOSICION.position.lat();

                        //    LISTA_LATLNG_RESULTS.push(MARKERPOSICION.position);
        //                    document.getElementById("address").value = direccion;
                            pintarCirculo(null,MARKERPOSICION.position);
                        }
                );
}

function geocodeAddress(direccion,map) {
  var address = direccion;
  var geocoder = new google.maps.Geocoder();
  var tabla =document.getElementById('tabladirecciones');
  tabla.innerHTML="";
  var codigohtml="";
  LISTA_LATLNG_RESULTS = [];
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
        for(var i=0;i<results.length;i++){
            LISTA_LATLNG_RESULTS.push(results[i].geometry.location);
        codigohtml+='<tr><td align="center" class="cadarow" onclick="pintarCirculo('+i+',null)">'+results[i].formatted_address+'</td></tr>';
    }
    tabla.innerHTML=codigohtml;
      //   console.log(results);


        return results[0].geometry.location;
    } else {
        alert("No se encontró nada a la direccion: "+address);
      return null;
    }
  });

}

//function generadorPopUp(image,nombre,codigo,placa,telefono,estado,direccion,hora,idtaxista){
function obtenerDireccionFromLatLng(latlng){

   var url ='http://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng.lat()+','+latlng.lng()+'&sensor=true';
  // console.log(url);
                $.ajax({
            url: url ,
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var direccion = data.results[0].formatted_address;
               var res = direccion.split(",");
               document.getElementById("address").value =res[0];
                DIRECCION_ACTUAL = res[0];
                LATLNG_ACTUAL =data.results[0].geometry.location;
        CURRENT_DIRECCION_Longitud = data.results[0].geometry.location.lng;
        CURRENT_DIRECCION_LATITUD = data.results[0].geometry.location.lat;
              return direccion;
            },
            error: function(data){
                return null;
            }
        });


}
function asignarCarrera(idtaxista,estado){
     var solicitudref = firebase.database().ref("solicitudes");
     var nuevasolicitud = solicitudref.push();
       var direccion = document.getElementById('address').value;
       var tmpciudad = CITYNOMBRE.split(",");
       console.log("empezando asignacion de carrera a ",idtaxista);
       var d = new Date();
      var MILLIS = d.getTime();


     nuevasolicitud.set({'ciudad':tmpciudad[0],
                         'direccion':direccion,
                     'idTaxista':idtaxista,
                         'latitud':CURRENT_DIRECCION_LATITUD,
                        'longitud':CURRENT_DIRECCION_LONGITUD,
                        'idUsuario':ID_EMPRESA,
                        'estado':estado,
                      'tipoSolicitud':'operadora',
                      'millisPedido':MILLIS});

                   // console.log("nuvea solicitud: "+nuevasolicitud);

     for(var i=0;i<LISTA_TAXIS.length;i++){
         var taxi = LISTA_TAXIS[i];
         if(taxi.ID_TAXISTA ===idtaxista){
            console.log("taxista encontrado en lista en la posicion ",i);
            if (estado === "aceptado"){
             taxi.asignarSolicitud(nuevasolicitud.key,idtaxista);
             taxi.updateMarker(taxi.LATLNG,taxi.iconFromEstado('asignado',taxi.RUMBO),taxi.ID_TAXI);
             //  alertify.alert('Ready!');
         taxi.POPUP.close();
      //  location.reload();

              $.notify("Carrera asignada a "+taxi.ID_TAXISTA+", recogerá pasajero en "+DIRECCION_ACTUAL,"success");
             //clearMarkers();
             //showMarkers();
           }else{
             taxi.SOLICITUD = new Solicitud(nuevasolicitud.key,taxi);
             console.log("esperando a que el taxista "+idtaxista+" responda a la solicitud");
           }
         }
     }

     DIRECCION_ACTUAL=null;
     esconderRuta();
     esconderPopUps();
     esconderCirculos();
}
// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < LISTA_TAXIS.length; i++) {
    LISTA_TAXIS[i].MARKER.setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}
function getPlaceLatLng(lugar){
    var request = {
    location: POPAYAN_LATLNG,
    radius: '10000',
    query: lugar
  };
    service.textSearch(request, callback);
}
function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {

    for (var i = 0; i < results.length; i++) {
      var place = results[i];
    //console.log(place);
    }
  }
}
function obtenerTaxisCercanos(latlng,rango){
  const RAD = 0.000008998719243599958; //1 METRO EN RADIANES
  var RADIO = RAD*rango;
  var LISTA_CERCANOS =[];
    var tmplista=[];
  for(var i=0;i<LISTA_TAXIS.length;i++){
  var taxi = LISTA_TAXIS[i];
  var calculo1 = Math.sqrt(Math.pow(taxi.LATITUD - latlng.lat(), 2) + Math.pow(taxi.LONGITUD - latlng.lng(), 2));

  if(calculo1<=RADIO){
    taxi.mostrarRuta(latlng);
//    taxi.tmp_DISTANCIA= measure(taxi.LATITUD,taxi.LONGITUD,latlng.lat(),latlng.lng());
  //  console.log("Taxi: "+taxi.ID_TAXISTA,"calculo: "+  calculo1,"radio: "+taxi.tmp_DISTANCIA);

    tmplista.push(taxi);
  }else{
  //  console.log("Taxi: "+taxi.ID_TAXISTA +" no esta en rango","calculo: "+calculo1,"radio: "+RADIO);
  }

  }
  if(tmplista.length>0){
    //En la lista existe un taxi al que no se le ha preguntado
    for(var t=0;t<tmplista.length;t++)
    LISTA_CERCANOS.push(tmplista[t]);

  //  console.log(LISTA_CERCANOS);
      //Organizamos el array de taxis por el taxi mas cercano
    LISTA_CERCANOS.sort(function(a, b) {
      return parseFloat(a.tmp_DISTANCIA_NAVEGACION) - parseFloat(b.tmp_DISTANCIA_NAVEGACION);
  });

  }else{
    //Se les preguntó a todos los taxis ya
    LISTA_CERCANOS=[];
//    console.log("tmplista",tmplista);
  }


//console.log(LISTA_CERCANOS);
  return LISTA_CERCANOS;
}
function measure(lat1, lon1, lat2, lon2){  // generally used geo measurement function
    var R = 6378.137; // Radius of earth in KM
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d * 1000; // meters
}
function cancelarSolicitud(){
  console.log("cancelando");
    RIPPLE_MARKER.setMap(null);
    SWITCH_ASIGNAR=false;
    var rootsol= firebase.database().ref("solicitudes").child(SOLICITUD_ACTUAL);
      rootsol.update({'estado':'cancelado'});
        $('#panelbuscando').hide();
}
